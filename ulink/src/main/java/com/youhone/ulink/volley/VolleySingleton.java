package com.youhone.ulink.volley;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class VolleySingleton {
	private static VolleySingleton mInstance;
	private RequestQueue mRequestQueue;
	private Context mContext;
	
	private VolleySingleton(Context context) {
		this.mContext = context;
		this.mRequestQueue = getRequestQueue();
	}
	
	public static VolleySingleton getInstance(Context context) {
		if (mInstance == null)
			synchronized (VolleySingleton.class) {
				if (mInstance == null)
					mInstance = new VolleySingleton(context);
			}
		return mInstance;
	}
	
	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(mContext.getApplicationContext());
		}
		return mRequestQueue;
	}
	
	public <T> void addToRequestQueue(Request<T> req) {
		getRequestQueue().add(req);
	}
	
}
