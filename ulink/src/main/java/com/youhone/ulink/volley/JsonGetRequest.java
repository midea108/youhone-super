package com.youhone.ulink.volley;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class JsonGetRequest extends Request<JSONObject> {
//	private Map<String, String> mMap;
	private Listener<JSONObject> mListener;

//	public JsonPostRequest(int method, String url, Map<String, String> map,
//			Listener<JSONObject> listener, ErrorListener errorListener) {
//		super(method, url, errorListener);
//		this.mMap = map;
//		this.mListener = listener;
//	}
//
//	public JsonPostRequest(String url, Map<String, String> map,
//			Listener<JSONObject> listener, ErrorListener errorListener) {
//		this(Method.POST, url, map, listener, errorListener);
//	}
	
	public JsonGetRequest(int method, String url, 
			Listener<JSONObject> listener, ErrorListener errorListener) {
		super(method, url, errorListener);
		System.out.println("请求的url：  " + url);
		this.mListener = listener;
	}

	public JsonGetRequest(String url, 
			Listener<JSONObject> listener, ErrorListener errorListener) {
		this(Method.GET, url, listener, errorListener);
	}


//	@Override
//	protected Map<String, String> getParams() throws AuthFailureError {
//		return mMap;
//	}

	@Override
	protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		try {
			String jsonString = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));
			
			//去掉两头的引号和其中的转义符
			String str = jsonString.replace("\\", "");
			str = str.substring(1,str.length()-1);
			System.out.println(str);
			
			return Response.success(new JSONObject(str),
					HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JSONException je) {
			return Response.error(new ParseError(je));
		}
	}

	@Override
	protected void deliverResponse(JSONObject response) {
		mListener.onResponse(response);
	}
}
