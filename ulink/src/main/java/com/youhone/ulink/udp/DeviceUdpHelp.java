package com.youhone.ulink.udp;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;


public class DeviceUdpHelp {
    private static final String TAG="UDP";
    private static final int PORT=8899;
    private static final String URL="";
    private Handler mHandler;
    private DatagramSocket socket = null;
    private DatagramPacket sendPacket;
    private DatagramPacket reveivePacket;
    private InetAddress dip;
    private int remotePort;
    private Thread re;
    private boolean start = true;
    Message msg;

    /**
     * 实例化
     *
     * @param uHandler
     */
    public DeviceUdpHelp(Handler uHandler, Context c) {
        this.mHandler = uHandler;
        start = true;
        msg = new Message();

        try {
            if (socket == null) {
                socket = new DatagramSocket(null);
                socket.setReuseAddress(true);
                socket.bind(new InetSocketAddress(PORT));
            }
            receive();
        } catch (SocketException e) {
            Log.d(TAG,"udp init failed----");
            e.printStackTrace();
        }

    }

    /**
     * 发送
     *
     * @param info
     * @param ip
     * @param port
     */
    public void send(JSONObject info, String ip, int port) {
        byte buff[] = info.toString().getBytes();
        try {
            dip = InetAddress.getByName(ip);
        } catch (UnknownHostException e1) {
            e1.printStackTrace();
            System.out.println("UnknownHostException----------" + e1);
        }
        remotePort = port;
        sendPacket = new DatagramPacket(buff, buff.length, dip, remotePort);
        System.out.println("send" + dip + "   " + remotePort);
        System.out.println("发送");
        System.out.println(new String(buff));
        try {
            socket.send(sendPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void scanBrocast(JSONObject info){
        byte buff[] = info.toString().getBytes();
        try {
            dip = InetAddress.getByName("255.255.255.255");
        } catch (UnknownHostException e1) {
            e1.printStackTrace();
            System.out.println("UnknownHostException----------" + e1);
        }
        DatagramPacket sendPacket = new DatagramPacket(buff, buff.length, dip, PORT);
        System.out.println("send" + dip + "   " + PORT);
        System.out.println("发送");
        System.out.println(new String(buff));
        try {
            socket.send(sendPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 接收线程
     *
     * @author Administrator
     */
    class EchoThread implements Runnable {
        public void run() {
            while (start) {
                byte[] buffer = new byte[256];
                reveivePacket = new DatagramPacket(buffer, buffer.length);
                try {
                    if (null != socket && !socket.isClosed()) {

                        socket.receive(reveivePacket);
                        byte[] receiveBuff = reveivePacket.getData();
                        System.out.println("接收");
                        for (byte b : receiveBuff) {
                            System.out.format("%02X ", b);
                        }
                        System.out.println("");
                        Message msg = new Message();
                        msg.obj = receiveBuff;
                        mHandler.sendMessage(msg);
                    }

                } catch (IOException e) {
                    System.out.println("UDP IOException----" + e);
                    e.printStackTrace();
                }
                // 清空
                reveivePacket = null;
                buffer = null;

            }

        }
    }

    /**
     * 启动线程
     */
    public void receive() {
        re = new Thread(new EchoThread());
        re.start();
    }

    public void close() {
        if (socket != null) {
            start = false;
            socket.close();
            socket = null;
        }
    }

}
