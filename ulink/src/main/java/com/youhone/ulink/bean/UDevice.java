package com.youhone.ulink.bean;

import java.io.Serializable;

/**
 * Created by cgt on 2016/4/11.
 * 设备的类
 */
public class UDevice implements Serializable{

    private String deviceName = "";//用户自定义的设备名字
    private String deviceMac = "";//设备唯一的mac地址
    private String ProductID = "";//设备的产品ID
    private int netState = 0;//设备的网络状态
    private String deviceLocalIP = "";//保存设备时的局域网IP
    private String deviceNetIP = "";//保存设备时的广域网IP
    private int devicePort = 1000;//设备的通信端口
    private String deviceID = "";//由后台分配的设备ID
    private boolean isOnline=false;//设备是否在线

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getDeviceMac() {
        return deviceMac;
    }

    public void setDeviceMac(String deviceMac) {
        this.deviceMac = deviceMac;
    }

    public String getProductID() {
        return ProductID;
    }

    public void setProductID(String productID) {
        ProductID = productID;
    }

    public int getNetState() {
        return netState;
    }

    public void setNetState(int netState) {
        this.netState = netState;
    }

    public String getDeviceLocalIP() {
        return deviceLocalIP;
    }

    public void setDeviceLocalIP(String deviceLocalIP) {
        this.deviceLocalIP = deviceLocalIP;
    }

    public String getDeviceNetIP() {
        return deviceNetIP;
    }

    public void setDeviceNetIP(String deviceNetIP) {
        this.deviceNetIP = deviceNetIP;
    }

    public int getDevicePort() {
        return devicePort;
    }

    public void setDevicePort(int devicePort) {
        this.devicePort = devicePort;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

}
