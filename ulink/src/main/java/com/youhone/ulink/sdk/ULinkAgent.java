package com.youhone.ulink.sdk;

import android.content.Context;
import android.os.Handler;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.youhone.ulink.bean.UDevice;
import com.youhone.ulink.listener.LinkToServerListener;
import com.youhone.ulink.listener.ULinkDeviceListener;
import com.youhone.ulink.statics.ULinkCode;
import com.youhone.ulink.statics.Url;
import com.youhone.ulink.udp.DeviceUdpHelp;
import com.youhone.ulink.udp.ServerUdpHelp;
import com.youhone.ulink.utils.HttpUtils;
import com.youhone.ulink.volley.JsonGetRequest;
import com.youhone.ulink.volley.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by cgt on 2016/4/11.
 * 全局分为两类监听，与服务器相关的LinkToServerListener，与设备相关的ULinkDeviceListener
 */
public class ULinkAgent {

    private static Context mContext;//上下文
    private final static String UDPVERSION = "udcp1.0";
    private final static int HEART = 30;
    private ServerUdpHelp mServerHelper = null;//用于与后台的UDP通信
    private DeviceUdpHelp mDeviceHelper = null;//用于与设备的UDP通信
    //心跳包的计时器
    private Timer timer;
    private TimerTask timerTask;
    private int time = 0;
    private List<LinkToServerListener> mServerList = new ArrayList<LinkToServerListener>();
    private List<ULinkDeviceListener> mDeviceListener = new ArrayList<ULinkDeviceListener>();
    /**
     * 维护一个设备列表
     */
    ArrayList<UDevice> devices = new ArrayList<UDevice>();

    private ULinkAgent() {
    }

    private static class LinkUtilsHolder {
        private static final ULinkAgent mInstance = new ULinkAgent();
    }

    public static ULinkAgent getInstance() {
        return LinkUtilsHolder.mInstance;
    }

    public static void init(Context c) {
        mContext = c;

    }


    public final Handler uiHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            byte[] receiveBuff = (byte[]) msg.obj;// 回来的数据
            String data = new String(receiveBuff);
            data = data.trim();
            System.out.println("json==" + data);
            try {
                JSONObject recevice = new JSONObject(data);
                int code = recevice.getInt("code");
                switch (code) {//根据code判断是什么类型的返回数据

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


    /**
     * 用户登录
     *
     * @param account  账号
     * @param password 密码
     */
    public void userLogin(final String account, String password) {

        //进行登录操作
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("account", account);
        map.put("psd", password);
        getJOFromServer( Url.Login(), map, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                //TODO 登录成功后
                JSONObject data = new JSONObject();
//                try {
//                    data.put("UserID", jsonObject.get("UserID"));
//                    data.put("version", UDPVERSION);
//                    startTimer(data);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                //返回信息给绑定的监听器
                for (int i = 0; i < mServerList.size(); i++) {
                    mServerList.get(i).onLogin(account, "token", "succeed", ULinkCode.SUCCEED);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //TODO 登录失败，返回对应的失败信息
                for (int i = 0; i < mServerList.size(); i++) {
                    mServerList.get(i).onLogin(account, "token", "failed", ULinkCode.FAID);
                }
            }
        });

    }


    public void userRegister(final String account, String password) {
        //进行注册操作
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("account", account);
        map.put("psd", password);
        getJOFromServer( "url", map, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                //TODO 注册成功后
                JSONObject data = new JSONObject();
                try {
                    data.put("UserID", jsonObject.get("UserID"));
                    data.put("version", UDPVERSION);
                    startTimer(data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //返回信息给绑定的监听器
                for (int i = 0; i < mServerList.size(); i++) {
                    mServerList.get(i).onRegister(account, "succeed", ULinkCode.SUCCEED);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                //TODO 注册失败，返回对应的失败信息
                for (int i = 0; i < mServerList.size(); i++) {
                    mServerList.get(i).onRegister(account, "failed", ULinkCode.FAID);
                }
            }
        });
    }

    /**
     * 注销登录
     *
     * @param account 用户名
     * @param token   凭证
     */
    public void removerUser(String account, String token) {
    }


    /**
     * 想服务器获取设备列表
     *
     * @param account      账号
     * @param access_token 凭证
     * @param PID          产品ID
     */
    public void getDeviceList(String account, String access_token, String... PID) {
//        HashMap<String, String> map = new HashMap<String, String>();
//        getJOFromServer("url", map, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject jsonObject) {
//                devices.clear();
//                //TODO 清空device后从回来的json得到新的列表，然后再调用局域网搜索更新列表的在线设备
//                for (int i = 0; i < mServerList.size(); i++) {
//                    mServerList.get(i).onGetDeviceList(devices, ULinkCode.SUCCEED);
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                for (int i = 0; i < mServerList.size(); i++) {
//                    mServerList.get(i).onGetDeviceList(null, ULinkCode.FAID);
//                }
//            }
//        });
    }

    /**
     * 根据一个产品ID扫描局域网的设备
     *
     * @param PID 产品ID
     */
    public ArrayList<UDevice> scanDeviceByProductId(String... PID) {
        JSONObject data = new JSONObject();
        for (String ip : PID) {
            try {
                data.put("dev_typ", ip);
                if (mDeviceHelper != null)
                    mDeviceHelper.scanBrocast(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //TODO 发现设备后调用mDeviceListener.onDeviceStatusChange，这里应该放在uihandler
        ArrayList<UDevice> devices = new ArrayList<UDevice>();
        return devices;
    }


    /**
     * 连接设备
     *
     * @param device 设备
     */
    public void connectToDevice(UDevice device) {
        //TODO 连接设备后调用mDeviceListener.onConnected
    }

    /**
     * 向设备发送信息
     *
     * @param data   数据包
     * @param device 设备
     */
    public void sendData(byte[] data, UDevice device) {
        //TODO 发送数据后调用mDeviceListener.onSendData
    }

    /**
     * 删除设备
     *
     * @param account 用户名
     * @param token   凭证
     * @param device  需要删除的设备
     */
    public void deleteDevice(String account, String token, UDevice device) {
    }

    /**
     * 重命名设备
     *
     * @param account 用户名
     * @param token   凭证
     * @param device  设备
     * @param newName 新名字
     * @return true:成功  false：失败
     */
    public boolean renameDevice(String account, String token, UDevice device, String newName) {
        return true;
    }


    /**
     * 添加与服务器连接的监听器
     *
     * @param listener
     */
    public void addServerListener(LinkToServerListener listener) {
        if (!mServerList.contains(listener))
            mServerList.add(listener);
    }

    /**
     * 添加与设备的监听
     */
    public void addDeviceListener(ULinkDeviceListener listener) {
        if (!mDeviceListener.contains(listener))
            mDeviceListener.add(listener);
    }

    /**
     * 移除服务器的监听器
     *
     * @param listener 监听器
     */
    public void removeServerListener(LinkToServerListener listener) {
        mServerList.remove(listener);
    }

    /**
     * 移除设备的监听器
     *
     * @param listener 监听器
     */
    public void removeDeviceListener(ULinkDeviceListener listener) {
        mDeviceListener.remove(listener);
    }

    /**
     * 移除所有设备监听器
     */
    public void removeAllDeviceListener() {
        mDeviceListener.clear();
    }

    /**
     * 移除所有服务器监听器
     */
    public void removeAllServerListener() {
        mServerList.clear();
    }

    /**
     * 连接至服务器
     */
    public void start() {
        //TODO 连接服务器，开始心跳包等操作
        if (mContext != null) {
            mServerHelper = new ServerUdpHelp(uiHandler, mContext);
            mDeviceHelper = new DeviceUdpHelp(uiHandler, mContext);
        }
    }


    /**
     * 程序结束时调用
     */
    public void stop() {
        //TODO 断开去服务器的连接，告知服务器下线，停止心跳包
        stopTimer();
        mServerHelper.close();
        mServerHelper = null;

    }


    public <T> void addToVolley(Request<T> request) {
//        if (System.currentTimeMillis() / 1000 - AccountStatic.loginTime >= AccountStatic.expireTime)
//            outOfTime();
//        else

        VolleySingleton.getInstance(mContext)
                .addToRequestQueue(request);
    }


    public void outOfTime() {

    }

    protected void getJOFromServer( String url, Map<String, String> map, Response.Listener<JSONObject> listner, Response.ErrorListener errorListener) {
        this.getJOFromServer( url, map, listner, errorListener, 0);
    }

    protected void postJOFromServer( String url, Map<String, String> map) {
        this.postJOFromServer( url, map, 0);
    }

    private boolean analyzeData(JSONObject json) {
        boolean result = false;
        try {
            result = json.getString("result").equalsIgnoreCase("True");
            if (!result) {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 从后台获取JsonObject
     *
     * @param url 请求的URL
     * @param map 发送的数据
     * @param tag TAG
     */
    protected void getJOFromServer(String url, Map<String, String> map, Response.Listener<JSONObject> listner, Response.ErrorListener errorListener, final int tag) {
        try {
            String resultUrl = URLDecoder.decode(url + HttpUtils.toGetParams(map), "UTF-8");

            JsonGetRequest request = new JsonGetRequest(resultUrl,
                    listner, errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                    return headers;
                }
            };
            addToVolley(request);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    /**
     * 从后台获取JsonObject
     *
     * @param url 请求的URL
     * @param map 发送的数据
     * @param tag TAG
     */
    protected void postJOFromServer(String url, final Map<String, String> map, final int tag) {

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        System.out.println("post方法返回" + response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return map;
            }
        };
        addToVolley(request);


    }

    protected void getJAFromServer(String url, Map<String, String> map) {
        this.getJAFromServer( url, map, 0);
    }

    /**
     * 从后台获取JsonArray
     *
     * @param url 请求的URL
     * @param map 发送的数据
     * @param tag TAG
     */
    protected void getJAFromServer( String url, Map<String, String> map, final int tag) {
        JsonArrayRequest request = new JsonArrayRequest(url + HttpUtils.toGetParams(map),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
                return headers;
            }
        };
        addToVolley(request);
    }


    /**
     * 启动定时器,开始倒计时
     */
    private void startTimer(final JSONObject data) {
        if (timer == null) {
            timer = new Timer();
        }
        if (timerTask == null) {
            timerTask = new TimerTask() {

                @Override
                public void run() {
                    if (mServerHelper != null) {
                        mServerHelper.send(data);
                    }

                }
            };
            if (timer != null && timerTask != null) {
                timer.schedule(timerTask, 1000, HEART * 1000);
            }
        }

    }

    /**
     * 停止定时器
     */
    private void stopTimer() {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


}
