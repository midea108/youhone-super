package com.youhone.ulink.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by cgt on 2016/4/11.
 */
public class HttpUtils {

    public static String toGetParams(Map<String, String> map) {
        String params = "";
        boolean isStart = true;
        for (String keyString : map.keySet()) {
            if (isStart) {
                params += '?';
                isStart = false;
            } else
                params += "&";
            params += (keyString + '=' + map.get(keyString));
        }
        return params;
    }

    public static JSONObject string2Json(String string) throws JSONException {
        if(string.startsWith("\"")){
            string = string.substring(1,string.length()-1);
        }
        string = string.replace("\\", "");
        JSONObject json = new JSONObject(string);

        return json;
    }


}
