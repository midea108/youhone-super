package com.youhone.ulink.listener;

import com.youhone.ulink.bean.UDevice;

import java.util.ArrayList;

/**
 * Created by cgt on 2016/4/11.
 * 全局与服务器连接的监听
 */
public interface LinkToServerListener {
    /**
     * 连接成功
     * @param code
     */
    public abstract void onSuccess(int code);

    /**
     * 连接失败
     * @param code
     */
    public abstract void onFail(int code);

    /**
     * 连接超时
     * @param code
     */
    public abstract void onTimeOut(int code);

    /**
     * 断开连接
     */
    public abstract void onDisConnect();

    /**
     * 用户登录
     */
    public abstract void onLogin(String account ,String access_token,String result,int code);

    /**
     * 用户注册
     */
    public abstract void onRegister(String account ,String result,int code);


    /**
     * 收到设备数据返回
     * @param data
     */
    public abstract void onReceiveData(byte[] data);

    /**
     * 获取到设备列表
     * @param devices
     * @param result
     */
    public abstract void onGetDeviceList(ArrayList<UDevice> devices,int result);

}
