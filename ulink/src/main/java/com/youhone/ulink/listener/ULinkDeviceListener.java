package com.youhone.ulink.listener;

import com.youhone.ulink.bean.UDevice;

/**
 * Created by cgt on 2016/4/12.
 */
public interface ULinkDeviceListener {
    /**
     * 设备发送数据时的监听
     * @param device
     * @param data
     * @param result
     */
    public abstract void onSendData(UDevice device,byte data[],int result);

    /**
     * 设备状态改变
     *
     * @param device
     * @param isOnline
     */
    public abstract void onDeviceStatusChange(UDevice device, boolean isOnline);

    /**
     * 设备连接成功
     *
     * @param device
     * @param result
     */
    public abstract void onConnected(UDevice device, int result);

    /**
     * 设备断开连接
     *
     * @param device
     */
    public abstract void onDisConnected(UDevice device);


    /**
     * 收到设备的信息
     *
     * @param data
     * @param device
     * @param result
     */
    public abstract void onReceiveData(byte[] data, UDevice device, int result);
}
