package com.youhone.youhone_super.welcome;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.youhone.superapp.R;
import com.youhone.youhone_super.AppApplication;
import com.youhone.youhone_super.activity.LoginActivity;
import com.youhone.youhone_super.base.BaseActivity;
import com.youhone.youhone_super.statics.AccountStatic;
import com.youhone.youhone_super.statics.Constant;
import com.youhone.youhone_super.utils.SharedPreferencesUtil;
import com.youhone.youhone_super.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cgt on 2016/4/11.
 */
public class WelcomeActivity extends BaseActivity {
    private static final int GOTO_MAINACTIVITY = 0;
    private static final int LOGIN_FAILED = 0;
    private static final int TIME = 3000;
    private static final int TIMEOUT = 6000;
    private boolean isRemember = false;
    private boolean loginFinish = false;
    private Intent intent;
    private Dialog mDialog;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_welcome);
    }

    @Override
    protected View initWidget() {
        AppApplication.sharedPreferences= getSharedPreferences(Constant.SHARE_NAME,
                Context.MODE_PRIVATE);
        return null;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        intent=new Intent();
        isRemember = checkLogin();
        if (isRemember) {//之前记住密码
            loginAuto();
        }
        mHandler.sendEmptyMessageDelayed(GOTO_MAINACTIVITY, TIME);
        mHandler.sendEmptyMessageDelayed(LOGIN_FAILED, TIMEOUT);
    }

    @Override
    protected void updateData(JSONObject json, int tag) {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
        if (json == null) {
            loginFinish = false;
            return;
        }
        loginFinish = analyzeData(json);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == GOTO_MAINACTIVITY) {
                if (isRemember && loginFinish) {//记住密码且登录成功，跳转到主页
                    intent2Main();
                } else if (isRemember && !loginFinish) {//记住密码但登录不成功，弹出对话框
                    showLoginDialog();
                } else if (!isRemember) {
                    intent2Login();
                }
            } else if (msg.what == LOGIN_FAILED) {
                if (null != mDialog && mDialog.isShowing())
                    mDialog.dismiss();
                if (isRemember && loginFinish) {
                    intent2Main();
                } else {
                    ToastUtils.shortToast("登录超时，请重新登录");
                    intent2Login();
                }

            }
            super.handleMessage(msg);
        }

    };


    private boolean checkLogin() {
        return SharedPreferencesUtil
                .queryBooleanValue(Constant.IS_REMEMBER);
    }

    private void loginAuto() {
        String account = SharedPreferencesUtil.queryValue(Constant.ACCOUNT);
        String password = SharedPreferencesUtil.queryValue(Constant.PASSWORD);
    }

    private boolean analyzeData(JSONObject json) {
        boolean result = false;
        try {
            AccountStatic.expireTime = Long.valueOf(json
                    .getString("expires_in"));
            AccountStatic.token = json.getString("access_token");
            SharedPreferencesUtil.keepShared(Constant.ACCOUNT, AccountStatic.userName);
            System.out.println("登录时的token" + json.getString("access_token"));
            result = json.getString("result").equalsIgnoreCase("True");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void intent2Login() {
        intent.setClass(WelcomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void intent2Main() {
        intent.setClass(WelcomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    private void showLoginDialog() {
        mDialog = ProgressDialog.show(WelcomeActivity.this, null, "登录中...");
        mDialog.setCancelable(false);
        mDialog.show();
    }


    @Override
    protected void onDestroy() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
