package com.youhone.youhone_super;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.youhone.ulink.sdk.ULinkAgent;
import com.youhone.youhone_super.statics.Constant;
import com.youhone.youhone_super.utils.LogUtils;


public class AppApplication extends Application {
    public static Context mContext;
    public static SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        ULinkAgent.init(this);
        sharedPreferences = getSharedPreferences(Constant.SHARE_NAME,
                Context.MODE_PRIVATE);
        init();
    }

    public static Context getContext() {
        return mContext;
    }


    private void init() {
        LogUtils.isDebug = true;
    }

}
