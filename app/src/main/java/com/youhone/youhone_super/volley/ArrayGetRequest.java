package com.youhone.youhone_super.volley;



import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;

public class ArrayGetRequest extends Request<JSONArray> {
//	private Map<String, String> mMap;
	private Response.Listener<JSONArray> mListener;

//	public ArrayPostRequest(int method, String url, Map<String, String> map,
//			Listener<JSONArray> listener, ErrorListener errorListener) {
//		super(method, url, errorListener);
//		this.mMap = map;
//		this.mListener = listener;
//	}
//
//	public ArrayPostRequest(String url, Map<String, String> map,
//			Listener<JSONArray> listener, ErrorListener errorListener) {
//		this(Method.POST, url, map, listener, errorListener);
//	}
	
	public ArrayGetRequest(int method, String url,
						   Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
		super(method, url, errorListener);
		this.mListener = listener;
	}

	public ArrayGetRequest(String url,
						   Response.Listener<JSONArray> listener, Response.ErrorListener errorListener) {
		this(Method.GET, url,  listener, errorListener);
	}

	// @Override
	// protected Map<String, String> getParams() throws AuthFailureError {
	// return mMap;
	// }

	@Override
	protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
		try {
			String jsonString = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));
			return Response.success(new JSONArray(jsonString),
					HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JSONException je) {
			return Response.error(new ParseError(je));
		}
	}

	@Override
	protected void deliverResponse(JSONArray response) {
		mListener.onResponse(response);
	}
}
