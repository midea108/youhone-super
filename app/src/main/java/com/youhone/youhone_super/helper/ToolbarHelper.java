package com.youhone.youhone_super.helper;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.youhone.youhone_super.AppApplication;



public class ToolbarHelper {
    private String title = "";
    private String subTitle = null;
    private int titleTextColor;
    private int backgroundColor;
    private int menuId;
    private Drawable navigationIcon;
    private Drawable logo;
    private Toolbar.OnMenuItemClickListener menuItemListener;
    private View.OnClickListener navigationListener;
    private boolean backBtnShow = false;

    public String getTitle() {
        return title;
    }

    public ToolbarHelper setTitle(int stringId) {
        this.title = AppApplication.getContext().getResources().getString(stringId);
        return this;
    }

    public ToolbarHelper setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public ToolbarHelper setSubTitle(int stringId) {
        this.subTitle = AppApplication.getContext().getResources().getString(stringId);
        return this;
    }

    public ToolbarHelper setSubTitle(String subTitle) {
        this.subTitle = subTitle;
        return this;
    }

    public int getTitleTextColor() {
        return titleTextColor;
    }

    public ToolbarHelper setTitleTextColor(int titleTextColor) {
        this.titleTextColor = titleTextColor;
        return this;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public ToolbarHelper setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public int getMenuId() {
        return menuId;
    }

    public ToolbarHelper setMenuId(int menuId) {
        this.menuId = menuId;
        return this;
    }

    public Toolbar.OnMenuItemClickListener getMenuItemListener() {
        return menuItemListener;
    }

    public ToolbarHelper setMenuItemListener(Toolbar.OnMenuItemClickListener menuItemListener) {
        this.menuItemListener = menuItemListener;
        return this;
    }

    public View.OnClickListener getNavigationListener() {
        return navigationListener;
    }

    public ToolbarHelper setNavigationListener(View.OnClickListener navigationListener) {
        this.navigationListener = navigationListener;
        return this;
    }

    public Drawable getLogo() {
        return logo;
    }

    public ToolbarHelper setLogo(int logoId) {
        this.logo = AppApplication.getContext().getResources().getDrawable(logoId);
        return this;
    }

    public ToolbarHelper setlogo(Drawable logo) {
        this.logo = logo;
        return this;
    }

    public Drawable getNavigationIcon() {
        return navigationIcon;
    }

    public ToolbarHelper setNavigationIcon(int iconId) {
        this.navigationIcon = AppApplication.getContext().getResources().getDrawable(iconId);
        return this;
    }

    public ToolbarHelper setNavigationIcon(Drawable navigationIcon) {
        this.navigationIcon = navigationIcon;
        return this;
    }

    public boolean isBackBtnShow() {
        return backBtnShow;
    }

    public ToolbarHelper setBackBtnShow(boolean backBtnShow) {
        this.backBtnShow = backBtnShow;
        return this;
    }
}
