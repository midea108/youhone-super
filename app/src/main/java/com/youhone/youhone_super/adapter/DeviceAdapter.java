package com.youhone.youhone_super.adapter;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.youhone.superapp.R;
import com.youhone.ulink.bean.UDevice;
import com.youhone.youhone_super.statics.Constant;

public class DeviceAdapter extends BaseAdapter {
    private ArrayList<UDevice> devices;
    private Context context;
    private int size;
    private int onlineCount = 0;

    public DeviceAdapter(Context context, ArrayList<UDevice> devices) {
        this.devices = devices;
        this.context = context;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return devices.size() + 1;
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public void setOnlineCount(int count) {
        this.onlineCount = count;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        size = devices.size();
        int actual_position = size - position - 1;
        if (position != size) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.device_list_item, null);
            holder.img = (ImageView) convertView.findViewById(R.id.deviceImg);
            holder.name = (TextView) convertView.findViewById(R.id.deviceName);
            holder.state = (TextView) convertView
                    .findViewById(R.id.deviceState);
            holder.list_item_layout = (RelativeLayout) convertView
                    .findViewById(R.id.list_item_layout);
            convertView.setTag(holder);

            holder.name.setText("");
            UDevice device = devices.get(actual_position);
            for (int i = 0; i < Constant.PRODUCTID.length; i++) {
                if (device.getProductID().equals(Constant.PRODUCTID[i])) {
                    holder.name.setText(Constant.PRODUCT_NAME[i]);
                    holder.img
                            .setImageResource(Constant.PRODUCT_ICO_OFFLINE[i]);
                    break;
                }
            }
            if (position < onlineCount) {
                for (int i = 0; i < Constant.PRODUCTID.length; i++) {
                    if (device.getProductID().equals(Constant.PRODUCTID[i])) {
                        holder.img
                                .setImageResource(Constant.PRODUCT_ICO_ONLINE[i]);
                        break;
                    }
                }
                holder.state.setText("在线");
                holder.state.setTextColor(context.getResources().getColor(
                        R.color.main_color));
                holder.name.setTextColor(context.getResources().getColor(
                        R.color.main_color));
                holder.list_item_layout
                        .setBackgroundResource(R.drawable.mm_listitem);
            } else {
                holder.state.setText("离线");
                holder.state.setTextColor(context.getResources().getColor(
                        R.color.main_gray));
                holder.name.setTextColor(context.getResources().getColor(
                        R.color.main_gray));
                holder.list_item_layout
                        .setBackgroundResource(R.drawable.mm_listitem);
                // holder.list_item_layout
                // .setBackgroundResource(R.drawable.mm_listitem_grey);
            }
            return convertView;
        } else {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.device_list_add_item, null);

            return convertView;
        }

    }

    public UDevice getDeviceByPosition(int position) {
        if (position - 1 < devices.size()) {
            if (devices.size() == 0) {
                return null;
            } else {
                return devices.get(size - (position - 1) - 1);
            }
        } else {
            return null;
        }

    }

    public boolean getState(int position) {
        return position < onlineCount;
    }
}

class ViewHolder {
    public ImageView img;
    public TextView name;
    public TextView state;
    public RelativeLayout list_item_layout;
    public TextView end_time;

}
