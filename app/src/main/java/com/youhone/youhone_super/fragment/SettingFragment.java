package com.youhone.youhone_super.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.youhone.superapp.R;
import com.youhone.youhone_super.activity.MainActivity;
import com.youhone.youhone_super.base.BaseFragment;
import com.youhone.youhone_super.update.UpdateUtil;


public class SettingFragment extends BaseFragment implements OnClickListener {
	private LinearLayout updata_lay, change_pw_lay, resport_lay, user_help,
			about_lay;
	private Button b_login_out;

	public SettingFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_setting, container, false);
		initView(view);
		return view;
	}

	private void initView(View view) {
		about_lay = (LinearLayout) view.findViewById(R.id.about_lay);
		user_help = (LinearLayout) view.findViewById(R.id.user_help);
		resport_lay = (LinearLayout) view.findViewById(R.id.resport_lay);
		updata_lay = (LinearLayout) view.findViewById(R.id.updata_lay);
		change_pw_lay = (LinearLayout) view.findViewById(R.id.change_pw_lay);
		b_login_out = (Button) view.findViewById(R.id.b_login_out);
		resport_lay.setOnClickListener(this);
		b_login_out.setOnClickListener(this);
		updata_lay.setOnClickListener(this);
		change_pw_lay.setOnClickListener(this);
		user_help.setOnClickListener(this);
		about_lay.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.updata_lay) {
			checkUpdata();
		} else if (id == R.id.change_pw_lay) {
		} else if (id == R.id.b_login_out) {
			getAct().showTipsDialog("确定退出当前帐号吗？",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
		} else if (id == R.id.resport_lay) {
			//startActivity(new Intent(getActivity(), ActivityResport.class));
		} else if (id == R.id.user_help) {
			//startActivity(new Intent(getActivity(), ActivityHelp.class));
		} else if (id == R.id.about_lay) {
			//startActivity(new Intent(getActivity(), ActivityAboutMain.class));
		}

	}

	private void checkUpdata() {
		UpdateUtil updateUtil = new UpdateUtil(getActivity());
		updateUtil.checkUpdate();
	}

	private MainActivity getAct() {
		return (MainActivity) getActivity();
	}
}
