package com.youhone.youhone_super.fragment;


import java.util.ArrayList;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.youhone.superapp.R;
import com.youhone.ulink.bean.UDevice;
import com.youhone.ulink.listener.LinkToServerListener;
import com.youhone.ulink.listener.ULinkDeviceListener;
import com.youhone.ulink.sdk.ULinkAgent;
import com.youhone.ulink.statics.ULinkCode;
import com.youhone.youhone_super.activity.MainActivity;
import com.youhone.youhone_super.adapter.DeviceAdapter;
import com.youhone.youhone_super.base.BaseActivity;
import com.youhone.youhone_super.base.BaseFragment;
import com.youhone.youhone_super.bean.LocalInfo;
import com.youhone.youhone_super.bean.NetType;
import com.youhone.youhone_super.statics.AccountStatic;
import com.youhone.youhone_super.statics.Constant;
import com.youhone.youhone_super.utils.LogUtils;
import com.youhone.youhone_super.weather.Weather_Detail;
import com.youhone.youhone_super.weatherform.WeatherForm;
import com.youhone.youhone_super.weatherquery.WeatherQueryManageImpl;


public class DeviceListFragment extends BaseFragment implements
        OnItemClickListener, OnItemLongClickListener,
        SwipeRefreshLayout.OnRefreshListener, LinkToServerListener, ULinkDeviceListener {
    /**
     * The Constant TAG.
     */
    private static final String TAG = "DeviceListFragment";
    private static final int SETLIST = 100;
    private static final int UPDATALIST = 101;
    private static final int TIMEOUT = 102;
    private static final int SETWEATHER = 103;
    private ListView lvDevices;
    private SwipeRefreshLayout mSwipeLayout;
    private DeviceAdapter mDeviceListAdapter;
    private Dialog progressDialog;
    private Dialog Intent_progressDialog;
    private boolean Refresh_Finish = false;
    // 天气相关
    private TextView city, temp, pm25, quality, date, week, home_shidu;
    private ImageView home_weather_img;
    private LinearLayout weather_data;
    private RelativeLayout weather_lay, list_lay;
    private TextView no_city_tip;
    private WeatherForm[] weathers;
    /**
     * 后台获取到的设备列表
     */
    ArrayList<UDevice> devices = new ArrayList<UDevice>();
    /**
     * 扫描到的设备列表，通过mac地址进行对比，从而得到后台获取到的设备的状态
     */
    ArrayList<UDevice> scan_devices = new ArrayList<UDevice>();

    @SuppressLint("HandlerLeak")
    private Handler uiHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SETWEATHER:
                    weathers = (WeatherForm[]) msg.obj;
                    if (weathers != null && weathers.length >= 1) {
                        System.out.println("天气显示成功1");
                        no_city_tip.setVisibility(View.GONE);
                        weather_data.setVisibility(View.VISIBLE);
                        WeatherForm firstDay = weathers[0];
                        System.out.println("天气显示成功2" + firstDay.toString());
                        city.setText(firstDay.getName());
                        temp.setText(firstDay.getCurrent_temp() + "℃");
                        date.setText(firstDay.getDdate());
                        week.setText("星期" + firstDay.getWeek());
                        pm25.setText(firstDay.getPm25());
                        String air_quality = firstDay.getQuality();
                        quality.setText("空气质量：" + air_quality);
                        if (air_quality.contains("优")) {
                            Constant.WEATHER_QUALITY = 1;
                            list_lay.setBackgroundResource(R.mipmap.pollution_low);
                            quality.setBackgroundResource(R.drawable.bg_quality_you);
                        } else if (air_quality.contains("良")) {
                            Constant.WEATHER_QUALITY = 2;
                            list_lay.setBackgroundResource(R.mipmap.pollution_low);
                            quality.setBackgroundResource(R.drawable.bg_quality_liang);
                        } else if (air_quality.contains("轻度")) {
                            Constant.WEATHER_QUALITY = 3;
                            quality.setBackgroundResource(R.drawable.bg_quality_qingdu);
                            list_lay.setBackgroundResource(R.mipmap.pollution_middle);
                        } else if (air_quality.contains("中度")) {
                            Constant.WEATHER_QUALITY = 4;
                            quality.setBackgroundResource(R.drawable.bg_quality_zhongdu);
                            list_lay.setBackgroundResource(R.mipmap.pollution_middle);
                        } else if (air_quality.contains("差")
                                || air_quality.contains("重度")) {
                            Constant.WEATHER_QUALITY = 5;
                            quality.setBackgroundResource(R.drawable.bg_quality_heavy);
                            list_lay.setBackgroundResource(R.mipmap.pollution_high);
                        } else if (air_quality.contains("差")
                                || air_quality.contains("严重")) {
                            Constant.WEATHER_QUALITY = 6;
                            quality.setBackgroundResource(R.drawable.bg_quality_yanzhong);
                            list_lay.setBackgroundResource(R.mipmap.pollution_high);
                        } else {
                            Constant.WEATHER_QUALITY = 1;
                            quality.setBackgroundResource(R.drawable.bg_quality_you);
                            list_lay.setBackgroundResource(R.mipmap.pollution_low);
                        }
                        home_shidu.setText(firstDay.getHumidity() + "%");
                        home_weather_img.setImageResource(firstDay.getImg_id());
                        weather_lay.setBackgroundResource(firstDay.getBg_id());
                        System.out.println("天气显示成功3" + firstDay.getTemp() + "℃");

                    } else {
                        weather_data.setVisibility(View.GONE);
                        no_city_tip.setVisibility(View.VISIBLE);
                    }
                    System.out.println("天气显示成功");
                    break;
                case SETLIST:// 获取到新的设备，重置适配器
                    Log.d(TAG, "列表设备长度  " + devices.size());
                    mDeviceListAdapter = new DeviceAdapter(getActivity(), devices);
                    lvDevices.setAdapter(mDeviceListAdapter);
                    break;
                case UPDATALIST:// 更新状态
                    System.out.println("updata   ");
                    int msize = devices.size();
                    int scan_size = scan_devices.size();
                    System.out.println("   scan_size" + scan_size + "    msize" + msize);
                    // 重新排序，在线的排在前面，背景为白色，离线的在下面，背景为灰色
                    int onlineCount = 0;
                    for (int i = 0; i < msize; i++) {
                        UDevice xdevice = devices.get(i);
                        String mac = xdevice.getDeviceMac();
                        for (int j = 0; j < scan_size; j++) {
                            if (mac.equals(scan_devices.get(j).getDeviceMac())) {// 在线
                                System.out.println(xdevice.getDeviceMac());
                                devices.remove(i);
                                devices.add(xdevice);
                                scan_devices.remove(j);
                                scan_size--;
                                i--;
                                msize--;
                                onlineCount++;
                                break;
                            }
                        }
                    }
                    mSwipeLayout.setRefreshing(false);
                    if (progressDialog.isShowing()) {
                        System.out.println("dismiss1");
                        progressDialog.dismiss();
                    }
                    mDeviceListAdapter.setOnlineCount(onlineCount);
                    mDeviceListAdapter.notifyDataSetChanged();
                    Refresh_Finish = true;


                    break;
                case TIMEOUT:
                    if (devices.size() == 0)
                        mSwipeLayout.setRefreshing(false);
                    // lvDevices.completeRefreshing();
                    if (progressDialog.isShowing()) {
                        System.out.println("dismiss2");
                        progressDialog.dismiss();
                    }

                    if (!Refresh_Finish) {
                        mDeviceListAdapter.setOnlineCount(0);
                        mDeviceListAdapter.notifyDataSetChanged();
                    }
                    Refresh_Finish = true;
                    break;

                default:
                    break;
            }

        }

        ;
    };

    public void onCreate(Bundle savedInstanceState) {
        Log("onCreate");
        super.onCreate(savedInstanceState);
    }

    public void onResume() {
        Log("onResume11");
        downLoadDevice();
        getWeather();
        super.onResume();
    }

    public void onDestroyView() {
        Log("onDestroyView");
        super.onDestroyView();
        devices.clear();
        scan_devices.clear();
        uiHandler.removeCallbacksAndMessages(null);
        lvDevices.setAdapter(null);
        ULinkAgent.getInstance().removeDeviceListener(this);
        ULinkAgent.getInstance().removeServerListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save away the original text, so we still have it if the activity
        // needs to be killed while paused.
        super.onSaveInstanceState(savedInstanceState);
        // downLoadDevice();
        if (null != Intent_progressDialog)
            Intent_progressDialog.dismiss();
        Log.e(TAG, "onSaveInstanceState");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_devicelist, container,
                false);
        Log("onCreateView");
        initDialog();
        quality = (TextView) view.findViewById(R.id.home_air_quality);
        date = (TextView) view.findViewById(R.id.home_weather_date);
        week = (TextView) view.findViewById(R.id.home_weather_week);
        city = (TextView) view.findViewById(R.id.home_city_name);
        temp = (TextView) view.findViewById(R.id.home_temp);
        pm25 = (TextView) view.findViewById(R.id.home_pm25);
        home_weather_img = (ImageView) view.findViewById(R.id.home_weather_img);
        home_shidu = (TextView) view.findViewById(R.id.home_shidu);
        no_city_tip = (TextView) view.findViewById(R.id.no_city_tip);
        weather_lay = (RelativeLayout) view.findViewById(R.id.weather_lay);
        list_lay = (RelativeLayout) view.findViewById(R.id.list_lay);
        weather_data = (LinearLayout) view.findViewById(R.id.weather_data);
        lvDevices = (ListView) view.findViewById(R.id.lvDevices);
        mSwipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.id_swipe_ly);
        weather_lay.setOnClickListener(new BtnListener());
        lvDevices.setOnItemClickListener(this);
        mDeviceListAdapter = new DeviceAdapter(getActivity(), devices);
        lvDevices.setAdapter(mDeviceListAdapter);
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setRefreshing(true);
        devices.clear();
        scan_devices.clear();
        ULinkAgent.getInstance().addServerListener(this);
        ULinkAgent.getInstance().addDeviceListener(this);
        ULinkAgent.getInstance().getDeviceList(AccountStatic.userName, AccountStatic.token, Constant.PRODUCTID);
        return view;
    }

    private void initDialog() {
        progressDialog = ((BaseActivity) getActivity())
                .createProgressDialog("", "正在获取设备列表，请稍等");
        Intent_progressDialog = ((BaseActivity) getActivity())
                .createProgressDialog("", "正在连接设备，请稍等");
        progressDialog.setCanceledOnTouchOutside(false);
        Intent_progressDialog.setCanceledOnTouchOutside(true);
        Intent_progressDialog.dismiss();
        progressDialog.dismiss();
    }

    private class BtnListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.weather_lay) {
                Intent intent = new Intent(getActivity(), Weather_Detail.class);
                intent.putExtra("weathers", weathers);
                startActivity(intent);
            }
        }

    }

    /**
     * 获取天气
     */
    private void getWeather() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                WeatherQueryManageImpl WQM = new WeatherQueryManageImpl();
                // UserCityManageImpl dbManage = UserCityManageImpl
                // .getDBInstance(getActivity());
                String city = getActivity()
                        .getSharedPreferences("city", Context.MODE_PRIVATE)
                        .getString(AccountStatic.userName, "").trim()
                        .toString();
                if (city.equals(""))
                    city = "广州";
                if (!city.equals("")) {
                    // 查询天气，返回3天的天气信息
                    msg.obj = WQM.weatherquery(city);
                    msg.what = SETWEATHER;
                    uiHandler.sendMessage(msg);
                }
            }
        }).start();
    }

    /**
     * 获取后台储存的设备
     */
    private void downLoadDevice() {
        Refresh_Finish = false;
        progressDialog.show();
        uiHandler.removeMessages(TIMEOUT);
        uiHandler.sendEmptyMessageDelayed(TIMEOUT, 12 * 1000);// 12S超时
        System.out.println("网络类型    " + LocalInfo.NetType);
        switch (LocalInfo.NetType) {
            case NetType.LAN:
            case NetType.NET:
                ULinkAgent.getInstance().getDeviceList(AccountStatic.userName, AccountStatic.token, Constant.PRODUCTID);
                break;
            default:
                break;
        }

    }

    @Override
    public void onSuccess(int code) {
        LogUtils.d("连接服务器成功");
    }

    @Override
    public void onFail(int code) {
        LogUtils.d("连接服务器失败");
    }

    @Override
    public void onTimeOut(int code) {
        LogUtils.d("连接服务器超时");
    }

    @Override
    public void onDisConnect() {

    }

    @Override
    public void onLogin(String account, String access_token, String result, int code) {

    }

    @Override
    public void onRegister(String account, String result, int code) {

    }

    @Override
    public void onReceiveData(byte[] data) {

    }

    @Override
    public void onGetDeviceList(ArrayList<UDevice> devices, int result) {
        if (result == ULinkCode.SUCCEED) {
            this.devices = devices;
            for (int i = 0; i < devices.size(); i++) {
                if (devices.get(i).isOnline() && !scan_devices.contains(devices.get(i))) {
                    scan_devices.add(devices.get(i));
                }
            }
            uiHandler.sendEmptyMessage(UPDATALIST);
        }

    }

    @Override
    public void onSendData(UDevice device, byte[] data, int result) {

    }

    /**
     * 设备状态改变
     * @param device
     * @param isOnline
     */
    @Override
    public void onDeviceStatusChange(UDevice device, boolean isOnline) {
        if (isOnline && !scan_devices.contains(device)) {
            scan_devices.add(device);
            uiHandler.sendEmptyMessage(UPDATALIST);
        } else if (!isOnline && scan_devices.contains(device)) {
            scan_devices.remove(device);
            uiHandler.sendEmptyMessage(UPDATALIST);
        }
    }

    /**
     * 连接设备成功
     * @param device
     * @param result
     */
    @Override
    public void onConnected(UDevice device, int result) {

    }

    /**
     * 与设备断开连接
     * @param device
     */
    @Override
    public void onDisConnected(UDevice device) {

    }

    @Override
    public void onReceiveData(byte[] data, UDevice device, int result) {

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }


    /**
     * 设备列表点击
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        if (position == mDeviceListAdapter.getCount() - 1) {
            return;
        }
        if (LocalInfo.NetType == NetType.NO_NET) {
            Toast.makeText(getActivity(), "当前无网络连接", Toast.LENGTH_SHORT).show();
            return;
        }
        UDevice tempDevice = mDeviceListAdapter
                .getDeviceByPosition(position + 1);
        if (tempDevice == null) {
            System.out.println("点击得到的device为空");
            return;
        }
        if (!mDeviceListAdapter.getState(position)) {
            Toast.makeText(getActivity(), "该设备离线", Toast.LENGTH_SHORT).show();
            for (int i = 0; i < Constant.PRODUCTID.length; i++) {
                if (tempDevice.getProductID().equals(Constant.PRODUCTID[i])) {
                    //TODO 打开设备控制界面
                    break;
                }
            }
            return;
        }
        Intent_progressDialog.show();
        Log.d(TAG, "点击的设备   " + tempDevice.getDeviceName());
    }


    private MainActivity getAct() {
        return (MainActivity) getActivity();
    }


    private void showDeleteDialog(final UDevice xdevice) {
        Builder builder = new Builder(getActivity());
        builder.setCancelable(false);
        builder.setMessage("确认删除该设备吗？");
        builder.setTitle("提示");
        builder.setNegativeButton("确定",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteDevice(xdevice.getDeviceMac());
                    }
                });
        builder.setNeutralButton("取消", null);
        builder.create().show();
    }

    /**
     * 删除设备
     *
     * @param mac
     */
    private void deleteDevice(final String mac) {

    }

    @Override
    public void onRefresh() {
        devices.clear();
        scan_devices.clear();
        mDeviceListAdapter = new DeviceAdapter(getActivity(), devices);
        lvDevices.setAdapter(mDeviceListAdapter);
        downLoadDevice();
    }

}
