package com.youhone.youhone_super.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.youhone.superapp.R;
import com.youhone.youhone_super.base.BaseFragment;


public class ShoppingFragment extends BaseFragment {
	private WebView main_page;

	public ShoppingFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_shopping, container, false);
		main_page = (WebView) view.findViewById(R.id.main_page);
		WebSettings webSettings = main_page.getSettings();
		webSettings.setDomStorageEnabled(true);
		webSettings.setAllowFileAccess(true);
		webSettings.setJavaScriptEnabled(true);
		webSettings.setLoadsImagesAutomatically(true);

		main_page.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
				view.loadUrl(url);
				return true;
			}
		});
		main_page
				.loadUrl("https://wap.koudaitong.com/v2/showcase/feature?alias=1i7ch079e");
		return view;
	}

	public boolean canGoBack() {
		return main_page != null && main_page.canGoBack();
	}

	public void goBack() {
		if (main_page != null) {
			System.out.println("ShoppingFragmetn--商城网页返回");
			main_page.goBack();
		}
	}

}
