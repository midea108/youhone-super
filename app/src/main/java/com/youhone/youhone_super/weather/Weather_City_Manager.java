package com.youhone.youhone_super.weather;


import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.youhone.superapp.R;
import com.youhone.youhone_super.base.BaseActivity;
import com.youhone.youhone_super.statics.AccountStatic;
import com.youhone.youhone_super.weatherform.WeatherForm;
import com.youhone.youhone_super.weatherquery.WeatherQueryManageImpl;

import org.apache.http.util.EncodingUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Weather_City_Manager extends BaseActivity {
    private ListView city_list;
    private ArrayList<String> datalist = new ArrayList<String>();
    private ArrayAdapter<String> cityAdapter;
    private EditText etSearch;
    private Button btnSearch;
    private Dialog progressDialog;
    private Handler uiHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            String text = etSearch.getText().toString();
            loadSomeCity(text);

        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        setContentView(R.layout.city_manager);
    }

    @Override
    protected View initWidget() {
        etSearch = (EditText) findViewById(R.id.etSearch);
        city_list = (ListView) findViewById(R.id.city_list);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        cityAdapter = new ArrayAdapter<String>(this,
                R.layout.city_item, datalist);
        loadAllCity();
        progressDialog = ProgressDialog.show(this, "", "正在获取天气，请稍等", true, true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.dismiss();
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                System.out.println("gaibi  " + s.toString());
                uiHandler.sendEmptyMessage(1);
            }
        });
        btnSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                loadSomeCity(etSearch.getText().toString());

            }
        });
        city_list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int positon, long arg3) {
                String city = datalist.get(positon);
                showTipsDialog("是否更换到" + city + "?", city);
            }
        });
        return null;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    private void loadAllCity() {
        datalist.clear();
        String res = "";
        InputStream in;
        try {
            in = getResources().getAssets().open("citycode.txt");
            int length = in.available();
            byte[] buffer = new byte[length];
            in.read(buffer);
            res = EncodingUtils.getString(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] ids = res.split("\n");
        for (String mid : ids) {
            // 如果城市没有添加过
            if (mid.split("=").length > 1) {
                datalist.add(mid.split("=")[1]);
            }
        }
        city_list.setAdapter(cityAdapter);
    }

    private void loadSomeCity(String text) {
        datalist.clear();
        String res = "";
        InputStream in;
        try {
            in = getResources().getAssets().open("citycode.txt");
            int length = in.available();
            byte[] buffer = new byte[length];
            in.read(buffer);
            res = EncodingUtils.getString(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] ids = res.split("\n");
        String city = "";
        for (String mid : ids) {

            if (mid.split("=").length > 1) {
                city = mid.split("=")[1];
                if (city.contains(text))
                    datalist.add(mid.split("=")[1]);
            }

        }
        cityAdapter.notifyDataSetChanged();
    }

    public void showTipsDialog(String msg, final String city) {
        Builder builder = new Builder(this);
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setTitle("提示");
        builder.setNegativeButton("确定",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog.show();
                        SharedPreferences sp = getSharedPreferences("city",
                                Context.MODE_PRIVATE);
                        Editor edt = sp.edit();
                        edt.putString(AccountStatic.userName, city);
                        edt.commit();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                final WeatherQueryManageImpl WQM = new WeatherQueryManageImpl();
                                // UserCityManageImpl dbManage =
                                // UserCityManageImpl
                                // .getDBInstance(getActivity());
                                final String city = getSharedPreferences("city",
                                        Context.MODE_PRIVATE)
                                        .getString(
                                                AccountStatic.userName,
                                                "").trim().toString();
                                if (!city.equals("")) {
                                    final WeatherForm[] weathers = WQM
                                            .weatherquery(city);
                                    // 查询天气，返回3天的天气信息
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressDialog.dismiss();
                                            setResult(RESULT_OK);
                                            finish();
                                        }
                                    });

                                }
                            }
                        }).start();

                    }
                });
        builder.setNeutralButton("取消", null);
        builder.create().show();
    }
}
