package com.youhone.youhone_super.weatherquery;


import com.youhone.youhone_super.weatherform.WeatherForm;

public interface WeatherQueryManage {
    /**
     * 查询天气方法
     *
     * @return 暂时返回3天的天气数据
     */
    public WeatherForm[] weatherquery(String CityName);

}
