package com.youhone.youhone_super.weatherquery;


import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.youhone.youhone_super.statics.Constant;
import com.youhone.youhone_super.weatherform.WeatherForm;


public class WeatherQueryManageImpl implements WeatherQueryManage {
	private final String TAG = "message";
	public static final String DEF_CHATSET = "UTF-8";
	public static final int DEF_CONN_TIMEOUT = 30000;
	public static final int DEF_READ_TIMEOUT = 30000;
	public static String userAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36";

	public WeatherForm[] weatherquery(String CityName) {

		String URL = "http://op.juhe.cn/onebox/weather/query?cityname="
				+ CityName + "&key=" + Constant.WEATHER_APPKEY;
		System.out.println(URL);
		String Weather_Result = "";
		HttpGet httpRequest = new HttpGet(URL);
		// 获得HttpResponse对象
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(httpRequest);
			System.out.println("返回+"
					+ httpResponse.getStatusLine().getStatusCode());
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 取得返回的数据
				Weather_Result = EntityUtils.toString(httpResponse.getEntity());
				Log.e("成功", Weather_Result);
			}
		} catch (Exception e) {

			Log.i(TAG, e.toString());

			return null;
		}
		// 以下是对返回JSON数据的解析
		if (null != Weather_Result && !"".equals(Weather_Result)) {
			try {
				JSONObject JO = new JSONObject(Weather_Result).getJSONObject(
						"result").getJSONObject("data");

				JSONArray JA = JO.getJSONArray("weather");
				// 取得正确数据的数目
				int j = 0;
				for (int i = 0; i < JA.length(); i++, j++) {
					try {
						WeatherForm weaf = new WeatherForm();
						JSONObject weather = ((JSONObject) JA.get(i));
						// System.out.println("weather========"
						// + weather.toString());
						JSONArray detail_day = weather.getJSONObject("info")
								.getJSONArray("day");
						try {
							JSONObject pm25 = JO.getJSONObject("pm25")
									.getJSONObject("pm25");
							// 3个日期暂时都存放一天的
							weaf.setPm25(pm25.getString("pm25"));
							weaf.setQuality(pm25.getString("quality"));
						} catch (JSONException e) {
							weaf.setPm25("暂无");
							weaf.setQuality("暂无");
							Log.i(TAG, e.toString());
						}
						// 3个日期暂时都存放一天的
						weaf.setName(JO.getJSONObject("realtime").getString(
								"city_name"));
						String[] data = weather.getString("date").split("-");
						if (data.length == 3) {
							weaf.setDdate(weather.getString("date").split("-")[1]
									+ "-"
									+ weather.getString("date").split("-")[2]);
						} else if (data.length == 2) {
							weaf.setDdate(weather.getString("date").split("-")[0]
									+ "-"
									+ weather.getString("date").split("-")[1]);
						} else {
							weaf.setDdate(weather.getString("date"));
						}
						JSONArray detail_night = weather.getJSONObject("info")
								.getJSONArray("night");
						weaf.setWeek(weather.getString("week"));
						weaf.setTemp(detail_night.getString(2) + "~"
								+ detail_day.getString(2));
						weaf.setCurrent_temp(detail_day.getString(2));
						weaf.setWind(detail_day.getString(3) + " "
								+ detail_day.getString(4));
						weaf.setWeather(detail_day.getString(1));
						weaf.setHumidity(JO.getJSONObject("realtime")
								.getJSONObject("weather").getString("humidity"));
					} catch (JSONException e) {
						Log.i(TAG, e.toString());
						break;
					}
				}
				WeatherForm[] WF = new WeatherForm[j];
				for (int i = 0; i < WF.length; i++) {

					WeatherForm weaf = new WeatherForm();

					JSONObject weather = ((JSONObject) JA.get(i));
					JSONArray detail_day = weather.getJSONObject("info")
							.getJSONArray("day");
					try {
						JSONObject pm25 = JO.getJSONObject("pm25")
								.getJSONObject("pm25");
						// 3个日期暂时都存放一天的
						weaf.setPm25(pm25.getString("pm25"));
						weaf.setQuality(pm25.getString("quality"));
					} catch (JSONException e) {
						weaf.setPm25("暂无");
						weaf.setQuality("暂无");
						Log.i(TAG, e.toString());
					}
					// 3个日期暂时都存放一天的
					weaf.setName(JO.getJSONObject("realtime").getString(
							"city_name"));
					String[] data = weather.getString("date").split("-");
					if (data.length == 3) {
						weaf.setDdate(weather.getString("date").split("-")[1]
								+ "-" + weather.getString("date").split("-")[2]);
					} else if (data.length == 2) {
						weaf.setDdate(weather.getString("date").split("-")[0]
								+ "-" + weather.getString("date").split("-")[1]);
					} else {
						weaf.setDdate(weather.getString("date"));
					}
					JSONArray detail_night = weather.getJSONObject("info")
							.getJSONArray("night");
					weaf.setWeek(weather.getString("week"));
					weaf.setTemp(detail_night.getString(2) + "~"
							+ detail_day.getString(2));
					weaf.setCurrent_temp(detail_day.getString(2));
					weaf.setWind(detail_day.getString(3) + " "
							+ detail_day.getString(4));
					weaf.setWeather(detail_day.getString(1));
					weaf.setHumidity(JO.getJSONObject("realtime")
							.getJSONObject("weather").getString("humidity"));
					// 首日取实时值
					if (i == 0) {
						JSONObject realtime = JO.getJSONObject("realtime");
						weaf.setTemp(realtime.getJSONObject("weather")
								.getString("temperature"));
						weaf.setCurrent_temp(realtime.getJSONObject("weather")
								.getString("temperature"));
						weaf.setHumidity(realtime.getJSONObject("weather")
								.getString("humidity"));
						weaf.setWind(realtime.getJSONObject("wind").getString(
								"direct")
								+ " "
								+ realtime.getJSONObject("wind").getString(
										"power"));
					}
					WF[i] = weaf;

				}
				JSONObject life_info = JO.getJSONObject("life").getJSONObject(
						"info");
				String tips = "1."
						+ life_info.getJSONArray("chuanyi").getString(1) + "\n"
						+ "2." + life_info.getJSONArray("ganmao").getString(1)
						+ "\n" + "3."
						+ life_info.getJSONArray("wuran").getString(1);
				WF[0].setTips(tips);
				return WF;
			} catch (JSONException e) {
				e.printStackTrace();
				System.out.println("========错误");
				Log.i(TAG, e.toString());

				return null;
			}
		}
		return null;
	}

	public ArrayList<WeatherForm[]> weatherqueryList(String[] CityNames) {
		ArrayList<WeatherForm[]> weatherlist = new ArrayList<WeatherForm[]>();
		for (String CityName : CityNames) {
			String URL = "http://op.juhe.cn/onebox/weather/query?cityname="
					+ CityName + "&key=" + Constant.WEATHER_APPKEY;
			System.out.println(URL);
			String Weather_Result = "";
			HttpGet httpRequest = new HttpGet(URL);
			// 获得HttpResponse对象
			try {
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse httpResponse = httpClient.execute(httpRequest);
				System.out.println("返回+"
						+ httpResponse.getStatusLine().getStatusCode());
				if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					// 取得返回的数据
					Weather_Result = EntityUtils.toString(httpResponse
							.getEntity());
					Log.e("成功", Weather_Result);
				}
			} catch (Exception e) {
				Log.i(TAG, e.toString());
				return null;
			}
			// 以下是对返回JSON数据的解析
			if (null != Weather_Result && !"".equals(Weather_Result)) {
				try {
					JSONObject JO = new JSONObject(Weather_Result)
							.getJSONObject("result").getJSONObject("data");

					JSONArray JA = JO.getJSONArray("weather");
					WeatherForm[] WF = new WeatherForm[JA.length()];
					for (int i = 0; i < WF.length; i++) {

						WeatherForm weaf = new WeatherForm();
						JSONObject weather = ((JSONObject) JA.get(i));

						JSONArray detail_day = weather.getJSONObject("info")
								.getJSONArray("day");
						try {
							JSONObject pm25 = JO.getJSONObject("pm25")
									.getJSONObject("pm25");
							// System.out.println("pm25======" +
							// pm25.toString());
							// 3个日期暂时都存放一天的
							weaf.setPm25(pm25.getString("pm25"));
							weaf.setQuality(pm25.getString("quality"));
						} catch (JSONException e) {
							weaf.setPm25("暂无");
							weaf.setQuality("暂无");
							Log.i(TAG, e.toString());
						}

						weaf.setName(JO.getJSONObject("realtime").getString(
								"city_name"));
						String[] data = weather.getString("date").split("-");
						if (data.length == 3) {
							weaf.setDdate(weather.getString("date").split("-")[1]
									+ "-"
									+ weather.getString("date").split("-")[2]);
						} else if (data.length == 2) {
							weaf.setDdate(weather.getString("date").split("-")[0]
									+ "-"
									+ weather.getString("date").split("-")[1]);
						} else {
							weaf.setDdate(weather.getString("date"));
						}
						JSONArray detail_night = weather.getJSONObject("info")
								.getJSONArray("night");
						weaf.setTemp(detail_night.getString(2) + "~"
								+ detail_day.getString(2));
						weaf.setWeek(weather.getString("week"));
						weaf.setWind(detail_day.getString(4));
						weaf.setWeather(detail_day.getString(1));
						weaf.setHumidity(JO.getJSONObject("realtime")
								.getJSONObject("weather").getString("humidity"));
						WF[i] = weaf;
					}
					weatherlist.add(WF);
				} catch (JSONException e) {
					e.printStackTrace();
					System.out.println("========错误");
					Log.i(TAG, e.toString());
					return null;
				}
			}
		}
		return weatherlist;
	}

}
