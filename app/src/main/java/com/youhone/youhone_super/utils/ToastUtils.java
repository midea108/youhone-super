package com.youhone.youhone_super.utils;

import android.widget.Toast;

import com.youhone.youhone_super.manager.AppManager;

public class ToastUtils {

    public static void shortToast(CharSequence text) {
        Toast.makeText(AppManager.getAppManager().currentActivity(), text, Toast.LENGTH_SHORT).show();
    }


    public static void longToast(CharSequence text) {
        Toast.makeText(AppManager.getAppManager().currentActivity(), text, Toast.LENGTH_LONG).show();
    }
}
