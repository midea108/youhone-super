package com.youhone.youhone_super.utils;

import android.content.SharedPreferences.Editor;

import com.youhone.youhone_super.AppApplication;

public class SharedPreferencesUtil {

    public static void keepShared(String key, String value) {
        Editor editor = AppApplication.sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void keepShared(String key, Integer value) {
        Editor editor = AppApplication.sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void keepShared(String key, float value) {
        Editor editor = AppApplication.sharedPreferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public static void keepShared(String key, long value) {
        Editor editor = AppApplication.sharedPreferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static float queryFloatValue(String key) {
        return AppApplication.sharedPreferences.getFloat(key, 0);
    }

    public static void keepShared(String key, int value) {
        Editor editor = AppApplication.sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * ������ѡ������
     *
     * @param key
     * @param value
     */
    public static void keepShared(String key, boolean value) {
        Editor editor = AppApplication.sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * ��ѯָ��key û�з���null
     *
     * @param key
     * @return
     */
    public static String queryValue(String key, String defvalue) {
        String value = AppApplication.sharedPreferences.getString(key, defvalue);
        // if ("".equals(value)) {
        // return "";
        // }

        return value;
    }

    /**
     * ��ѯָ��key û�з���null
     *
     * @param key
     * @return
     */
    public static String queryValue(String key) {
        String value = AppApplication.sharedPreferences.getString(key, "");
        if ("".equals(value)) {
            return "";
        }

        return value;
    }

    public static Integer queryIntValue(String key) {
        int value = AppApplication.sharedPreferences.getInt(key, 0);

        return value;
    }

    /**
     * @param key
     * @return
     */
    public static boolean queryBooleanValue(String key) {
        LogUtils.e(AppApplication.sharedPreferences == null ? "true" : "false");
        return AppApplication.sharedPreferences.getBoolean(key, false);
    }

    /**
     * @param key
     * @return
     */
    public static long queryLongValue(String key) {
        return AppApplication.sharedPreferences.getLong(key, 0);
    }

    /**
     * �������
     *
     * @return
     */
    public static boolean deleteAllValue() {

        return AppApplication.sharedPreferences.edit().clear().commit();
    }

    /**
     * ����Key�����
     *
     * @param key
     */
    public static void deleteValue(String key) {
        AppApplication.sharedPreferences.edit().remove(key).commit();
    }
}
