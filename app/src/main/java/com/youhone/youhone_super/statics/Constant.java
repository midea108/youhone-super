package com.youhone.youhone_super.statics;

public class Constant {

    // 默认密码
    public static final int password = 8888;
    // 企业ID
    public static final String corp_id = "youhone";
    // 产品id

    public static String PRODUCTID[];
    // TODO 产品默认名称
    public static String PRODUCT_NAME[];
    // 产品图标(在线)
    public static int PRODUCT_ICO_ONLINE[];
    // 产品图标(离线)
    public static int PRODUCT_ICO_OFFLINE[];
    // 天气质量
    public static int WEATHER_QUALITY;
    // 天气APPKEY
    public static String WEATHER_APPKEY;
    public static final String SHARE_NAME = "youhone_super_sp";
    public static final String USER_ID = "user_id";
    public static final String ACCOUNT = "account";
    public static final String PASSWORD = "password";
    public static final String IS_REMEMBER = "remember";
    /**
     * 成功
     */
    public static final int HTTP_SUCCEED = 100;
    /**
     * 失败
     */
    public static final int HTTP_FAILURE = 201;

    // TODO-----------------------各大数据表-------------------------
    // 用户表
    public static String TABLE_USER = "";
}
