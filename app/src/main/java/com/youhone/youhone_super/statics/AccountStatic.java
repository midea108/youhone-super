package com.youhone.youhone_super.statics;

import java.util.List;


public class AccountStatic {

	// 登录时间
	public static long loginTime;

	// 超时上限
	public static long expireTime = Long.MAX_VALUE;

	// 用户名
	public static String userName;

	// 身份标识
	public static String token;

}
