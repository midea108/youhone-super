package com.youhone.youhone_super.statics;

import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class MapUtils {
    private static HashMap<String, String> mMap;

    public static HashMap<String, String> getMap() {
        if (mMap == null || AccountStatic.userName == null || AccountStatic.token == null) {
            mMap = new HashMap<String, String>();
            mMap.put("Account", String.valueOf(AccountStatic.userName));
            mMap.put("access_token", AccountStatic.token);
        } else {
            mMap.clear();
            mMap.put("Account", String.valueOf(AccountStatic.userName));
            mMap.put("access_token", AccountStatic.token);
        }
        return mMap;
    }


}
