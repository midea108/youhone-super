package com.youhone.youhone_super.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.youhone.superapp.R;
import com.youhone.ulink.bean.UDevice;
import com.youhone.ulink.listener.LinkToServerListener;
import com.youhone.ulink.listener.ULinkDeviceListener;
import com.youhone.ulink.sdk.ULinkAgent;
import com.youhone.ulink.statics.ULinkCode;
import com.youhone.youhone_super.helper.ToolbarHelper;
import com.youhone.youhone_super.manager.AppManager;
import com.youhone.youhone_super.statics.AccountStatic;
import com.youhone.youhone_super.utils.LogUtils;
import com.youhone.youhone_super.utils.UiUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by cgt on 2016/4/11.
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static int mStatusBarHeight;
    private Activity currectActivity = null;
    //Toolbar ，may be null
    private Toolbar mBar;
    private ToolbarHelper mToolbarHelper;
    private static AlertDialog mDialog;
    //当前加了padding的View（即最顶部的View）
    private View topView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 竖屏锁定
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setStatusBarForVersion21Up();
        AppManager.getAppManager().addActivity(this);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN
                        | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ULinkAgent.getInstance().addDeviceListener(mDeviceListener);
        ULinkAgent.getInstance().addServerListener(mServerListener);
        initView();
        View topView = initWidget();
        initData(savedInstanceState);
        setTopPadding(topView);
    }

    @Override
    protected void onResume() {
        SharedPreferences sp = getSharedPreferences("account_info", Context.MODE_PRIVATE);
        AccountStatic.userName = sp.getString("userName", "");
        AccountStatic.token = sp.getString("token", "");
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().finishActivity(this);
    }

    /**
     * The method to initialize the view
     */
    protected abstract void initView();

    /**
     * The method to initialize the widgets
     *
     * @return The top view of this Activity,in order to add a padding as the same height as status bar when the version of the phone sdk is KitKat or up
     */
    protected abstract View initWidget();


    /**
     * The method to initialize data
     *
     * @param savedInstanceState The bundle in onCreate(Bundle)
     */
    protected abstract void initData(Bundle savedInstanceState);

    protected void doLogin(String account, String password) {
        ULinkAgent.getInstance().userLogin(account, password);
    }

    protected void LoginSucceed(String account, String access_token) {
    }

    ;


    private LinkToServerListener mServerListener = new LinkToServerListener() {
        @Override
        public void onSuccess(int code) {

        }

        @Override
        public void onFail(int code) {

        }

        @Override
        public void onTimeOut(int code) {

        }

        @Override
        public void onDisConnect() {

        }

        @Override
        public void onLogin(String account, String access_token, String result, int code) {
            if (code == ULinkCode.SUCCEED) {
                LoginSucceed(account, access_token);
            }
        }

        @Override
        public void onRegister(String account, String result, int code) {

        }


        @Override
        public void onReceiveData(byte[] data) {

        }

        @Override
        public void onGetDeviceList(ArrayList<UDevice> devices, int result) {

        }
    };


    private ULinkDeviceListener mDeviceListener = new ULinkDeviceListener() {
        @Override
        public void onSendData(UDevice device, byte[] data, int result) {

        }

        @Override
        public void onDeviceStatusChange(UDevice device, boolean isOnline) {

        }

        @Override
        public void onConnected(UDevice device, int result) {

        }

        @Override
        public void onDisConnected(UDevice device) {

        }

        @Override
        public void onReceiveData(byte[] data, UDevice device, int result) {

        }
    };


    /**
     * @return the initialization of toolbar
     */
    public Toolbar getToolbar() {
        return mBar;
    }

    public void setToolbar(ToolbarHelper helper, Activity c) {
        this.mToolbarHelper = helper;
        this.currectActivity = c;
        if (mBar == null) {
            mBar = (Toolbar) findViewById(R.id.toolbar);
        }

        if (helper != null) {
            initToolbar();
        } else {
            mBar.setTitle("");
        }


        if (getSupportActionBar() == null && mBar != null)
            setSupportActionBar(mBar);
    }

    private void setStatusBarForVersion21Up() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow()
                    .getDecorView()
                    .setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }

    public void setTopPadding(View topView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (mStatusBarHeight == 0) {
                mStatusBarHeight = UiUtils.getStatusBarHeight(this);
            }

            if (topView != null || (mBar != null && mBar.getVisibility() == View.VISIBLE)) {
                if (this.topView != null) {
                    this.topView.setPadding(this.topView.getPaddingLeft(), this.topView.getPaddingTop() - mStatusBarHeight,
                            this.topView.getPaddingRight(), this.topView.getPaddingBottom());
                }

                this.topView = (topView != null ? topView : mBar);
                this.topView.setPadding(this.topView.getPaddingLeft(), this.topView.getPaddingTop() + mStatusBarHeight,
                        this.topView.getPaddingRight(), this.topView.getPaddingBottom());
            }
        }
    }

    public void setStatusBarSpace() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (mStatusBarHeight == 0) {
                mStatusBarHeight = UiUtils.getStatusBarHeight(this);
            }
            View statusBarSpace = findViewById(R.id.status_bar_space);
            if (statusBarSpace != null) {
                statusBarSpace.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, mStatusBarHeight));
            } else {
                LogUtils.e("The layout of StatusBarSpace doesn't add in activity.");
            }
        }
    }

    private void initToolbar() {
        if (mBar != null) {
            mBar.setTitle(mToolbarHelper.getTitle());
            mBar.setSubtitle(mToolbarHelper.getSubTitle());
            if (mToolbarHelper.getTitleTextColor() != 0) {
                mBar.setTitleTextColor(mToolbarHelper.getTitleTextColor());
            }
            if (mToolbarHelper.getBackgroundColor() != 0) {
                mBar.setBackgroundColor(mToolbarHelper.getBackgroundColor());
            }
            mBar.setLogo(mToolbarHelper.getLogo());

            setSupportActionBar(mBar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(mToolbarHelper.isBackBtnShow());
            if (mToolbarHelper.getNavigationIcon() != null) {
                mBar.setNavigationIcon(mToolbarHelper.getNavigationIcon());
            }

            if (mToolbarHelper.getNavigationListener() != null) {
                mBar.setNavigationOnClickListener(mToolbarHelper.getNavigationListener());
            } else if (mToolbarHelper.isBackBtnShow()) {
                mBar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != currectActivity)
                            currectActivity.finish();
                    }
                });
            }

            mBar.setOnMenuItemClickListener(mToolbarHelper.getMenuItemListener());

            invalidateOptionsMenu();
        }
    }

    public void showToolbar() {
        if (mBar.getVisibility() == View.GONE) {
            mBar.setVisibility(View.VISIBLE);
            setTopPadding(mBar);
        }
    }

    public void hideToolbar() {
        if (mBar.getVisibility() == View.VISIBLE) {
            mBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLowMemory() {
        System.out.println("内存低");
        System.gc();
        super.onLowMemory();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mToolbarHelper != null && mToolbarHelper.getMenuId() != 0) {
            getMenuInflater().inflate(mToolbarHelper.getMenuId(), menu);
            return true;
        }
        return false;
    }

//    public <T> void addToVolley(Request<T> request) {
//        if (System.currentTimeMillis() / 1000 - AccountStatic.loginTime >= AccountStatic.expireTime)
//            outOfTime();
//        else
//            VolleySingleton.getInstance(getApplicationContext())
//                    .addToRequestQueue(request);
//    }
//
//
//    public void outOfTime() {
//
//    }
//
    protected void updateData(JSONObject json, int tag) {
    }

    protected void updateData(JSONArray json, int tag) {
    }

//    protected void getJOFromServer(String url, Map<String, String> map) {
//        this.getJOFromServer(url, map, 0);
//    }
//
//    protected void postJOFromServer(String url, Map<String, String> map) {
//        this.postJOFromServer(url, map, 0);
//    }
//
//    private boolean analyzeData(JSONObject json) {
//        boolean result = false;
//        try {
//            result = json.getString("result").equalsIgnoreCase("True");
//            if (!result) {
//
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return true;
//    }
//
//    /**
//     * 从后台获取JsonObject
//     *
//     * @param url
//     * @param map
//     * @param tag
//     */
//    protected void getJOFromServer(String url, Map<String, String> map, final int tag) {
//        try {
//            String resultUrl = URLDecoder.decode(url + toGetParams(map), "UTF-8");
//
//            JsonGetRequest request = new JsonGetRequest(resultUrl,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            if (analyzeData(response))
//                                updateData(response, tag);
//                        }
//                    }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                    updateData((JSONObject) null, tag);
//                }
//            }) {
//                @Override
//                public Map<String, String> getHeaders() throws AuthFailureError {
//                    HashMap<String, String> headers = new HashMap<String, String>();
//                    headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
//                    return headers;
//                }
//            };
//            addToVolley(request);
//        } catch (UnsupportedEncodingException e) {
//            Toast.makeText(this, "字符编码错误", Toast.LENGTH_SHORT).show();
//        }
//
//    }
//
//
//    /**
//     * 从后台获取JsonObject
//     *
//     * @param url
//     * @param map
//     * @param tag
//     */
//    protected void postJOFromServer(String url, final Map<String, String> map, final int tag) {
//
//        StringRequest request = new StringRequest(Request.Method.POST, url,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        try {
//                            JSONObject json = string2Json(response);
//                            if (analyzeData(json))
//                                updateData(json, tag);
//                        } catch (JSONException e) {
//                            System.out.println("json格式错误");
//                        }
//
//                        System.out.println("post方法返回" + response);
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//                updateData((JSONObject) null, tag);
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//
//                return map;
//            }
//        };
//        addToVolley(request);
//
//
//    }
//
//    protected void getJAFromServer(String url, Map<String, String> map) {
//        this.getJAFromServer(url, map, 0);
//    }
//
//    /**
//     * 从后台获取JsonArray
//     *
//     * @param url
//     * @param map
//     * @param tag
//     */
//    protected void getJAFromServer(String url, Map<String, String> map, final int tag) {
//        JsonArrayRequest request = new JsonArrayRequest(url + toGetParams(map),
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        updateData(response, tag);
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//                updateData((JSONArray) null, tag);
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
//                return headers;
//            }
//        };
//        addToVolley(request);
//    }
//
//    private String toGetParams(Map<String, String> map) {
//        String params = "";
//        boolean isStart = true;
//        for (String keyString : map.keySet()) {
//            if (isStart) {
//                params += '?';
//                isStart = false;
//            } else
//                params += "&";
//
//            params += (keyString + '=' + map.get(keyString));
//        }
//        return params;
//    }


    public void showDialog(String title, String msg, String NegMsg,
                           DialogInterface.OnClickListener NegListener, String PosMsg,
                           DialogInterface.OnClickListener PosListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);

        if (NegListener != null)
            builder.setNegativeButton(NegMsg, NegListener);
        if (PosListener != null)
            builder.setPositiveButton(PosMsg, PosListener);

        mDialog = builder.create();
        mDialog.show();
    }

    public static JSONObject string2Json(String string) throws JSONException {
        if (string.startsWith("\"")) {
            string = string.substring(1, string.length() - 1);
        }

        string = string.replace("\\", "");

        JSONObject json = new JSONObject(string);

        return json;
    }


    public Dialog createProgressDialog(String title, String tips) {
        ProgressDialog progressDialog = ProgressDialog.show(this, title,
                tips, true, true);
        progressDialog.setCanceledOnTouchOutside(false);

        return progressDialog;
    }


}
