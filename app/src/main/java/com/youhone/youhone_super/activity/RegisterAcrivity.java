package com.youhone.youhone_super.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.youhone.superapp.R;
import com.youhone.youhone_super.base.BaseActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by cgt on 2016/4/13.
 */
public class RegisterAcrivity extends BaseActivity implements View.OnClickListener {

    private TextView tv_reg_info_phone;
    private EditText
            et_reg_phone, et_reg_verify;
    private Button b_reg_phone, b_reg_get_verify,
            b_haveAccount;
    private EditText phone_reg_pwd, phone_reg_repwd, phone_reg_username;
    private CheckBox Purchannels1, Purchannels2, Purchannels3;
    private CheckBox InfoSources1, InfoSources2, InfoSources3, InfoSources4,
            InfoSources5, InfoSources6, InfoSources7, InfoSources8,
            InfoSources9;
    private CheckBox[] Purchannels;
    private CheckBox[] InfoSources;
    private PurchannelsCheckListener listener1 = new PurchannelsCheckListener();
    private InfoSourcesCheckListener listener2 = new InfoSourcesCheckListener();
    private int Purchannel = 0;
    private int InfoSource = 0;
    private String phoneNum;
    private int getCount = 0;
    private String phone_name, phone_psd;
    private Timer timer;
    private TimerTask timerTask;
    private int verify_time = 125;
    private TextView address_txt;
    // 定位相关
    private LocationManager locationManager;
    private Criteria criteria;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_register);
    }

    @Override
    protected View initWidget() {
        Purchannels = new CheckBox[3];
        InfoSources = new CheckBox[9];
        Purchannels1 = (CheckBox) findViewById(R.id.Purchannels1);
        Purchannels2 = (CheckBox) findViewById(R.id.Purchannels2);
        Purchannels3 = (CheckBox) findViewById(R.id.Purchannels3);
        Purchannels[0] = Purchannels1;
        Purchannels[1] = Purchannels2;
        Purchannels[2] = Purchannels3;
        for (int i = 0; i < Purchannels.length; i++) {
            Purchannels[i].setOnCheckedChangeListener(listener1);
        }
        InfoSources1 = (CheckBox) findViewById(R.id.InfoSources1);
        InfoSources2 = (CheckBox) findViewById(R.id.InfoSources2);
        InfoSources3 = (CheckBox) findViewById(R.id.InfoSources3);
        InfoSources4 = (CheckBox) findViewById(R.id.InfoSources4);
        InfoSources5 = (CheckBox) findViewById(R.id.InfoSources5);
        InfoSources6 = (CheckBox) findViewById(R.id.InfoSources6);
        InfoSources7 = (CheckBox) findViewById(R.id.InfoSources7);
        InfoSources8 = (CheckBox) findViewById(R.id.InfoSources8);
        InfoSources9 = (CheckBox) findViewById(R.id.InfoSources9);
        InfoSources[0] = InfoSources1;
        InfoSources[1] = InfoSources2;
        InfoSources[2] = InfoSources3;
        InfoSources[3] = InfoSources4;
        InfoSources[4] = InfoSources5;
        InfoSources[5] = InfoSources6;
        InfoSources[6] = InfoSources7;
        InfoSources[7] = InfoSources8;
        InfoSources[8] = InfoSources9;
        for (int i = 0; i < InfoSources.length; i++) {
            InfoSources[i].setOnCheckedChangeListener(listener2);
        }
        address_txt = (TextView) findViewById(R.id.address_txt);
        b_haveAccount = (Button) findViewById(R.id.b_haveAccount);
        b_haveAccount.setOnClickListener(this);
        tv_reg_info_phone = (TextView) findViewById(R.id.tv_reg_info_phone);
        et_reg_phone = (EditText) findViewById(R.id.et_reg_phone_num);
        et_reg_verify = (EditText) findViewById(R.id.et_reg_verify);
        phone_reg_pwd = (EditText) findViewById(R.id.phone_reg_pwd);
        phone_reg_repwd = (EditText) findViewById(R.id.phone_reg_repwd);
        phone_reg_username = (EditText) findViewById(R.id.phone_reg_username);
        b_reg_get_verify = (Button) findViewById(R.id.b_get_verify);
        b_reg_phone = (Button) findViewById(R.id.b_reg_phone);
        b_reg_get_verify.setOnClickListener(this);
        b_reg_phone.setOnClickListener(this);
        // initLocation();
        String serviceName = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) getSystemService(
                serviceName);
        String provider = LocationManager.NETWORK_PROVIDER;
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
        try {
            locationManager.requestLocationUpdates(provider, 2000, 10,
                    locationListener);
        } catch (SecurityException e) {
            e.printStackTrace();
            address_txt.setText("无法获取位置信息");
        }

        return null;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }


    private class PurchannelsCheckListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            if (isChecked) {
                switch (buttonView.getId()) {
                    case R.id.Purchannels1:
                        Purchannel = 1;
                        break;
                    case R.id.Purchannels2:
                        Purchannel = 2;
                        break;
                    case R.id.Purchannels3:
                        Purchannel = 3;
                        break;
                    default:
                        break;
                }
            } else {
                Purchannel = 0;
            }
            changePurchannelsStatus();
        }

    }

    private class InfoSourcesCheckListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            if (isChecked) {
                switch (buttonView.getId()) {
                    case R.id.InfoSources1:
                        InfoSource = 1;
                        break;
                    case R.id.InfoSources2:
                        InfoSource = 2;
                        break;
                    case R.id.InfoSources3:
                        InfoSource = 3;
                        break;
                    case R.id.InfoSources4:
                        InfoSource = 4;
                        break;
                    case R.id.InfoSources5:
                        InfoSource = 5;
                        break;
                    case R.id.InfoSources6:
                        InfoSource = 6;
                        break;
                    case R.id.InfoSources7:
                        InfoSource = 7;
                        break;
                    case R.id.InfoSources8:
                        InfoSource = 8;
                        break;
                    case R.id.InfoSources9:
                        InfoSource = 9;
                        break;
                    default:
                        break;
                }
            } else {
                InfoSource = 0;
            }
            changeInfoSourcesStatus();
        }

    }

    private void changePurchannelsStatus() {
        for (int i = 0; i < Purchannels.length; i++) {
            if (i != Purchannel - 1)
                Purchannels[i].setChecked(false);
            else
                Purchannels[i].setChecked(true);
        }
    }

    private void changeInfoSourcesStatus() {
        for (int i = 0; i < InfoSources.length; i++) {
            if (i != InfoSource - 1)
                InfoSources[i].setChecked(false);
            else
                InfoSources[i].setChecked(true);
        }
    }

    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(final Location location) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    updateWithNewLocation(location);
                }
            }).start();
        }

        public void onProviderDisabled(String provider) {
            updateWithNewLocation(null);
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    private void updateWithNewLocation(Location location) {
        String latLongString;
        if (location != null) {
            final double lat = location.getLatitude();
            final double lng = location.getLongitude();
            latLongString = "纬度:" + lat + "\n经度:" + lng;
            String url = "http://recode.ditu.aliyun.com/dist_query?l=" + lat
                    + "," + lng;
            HttpGet get = new HttpGet(url);
            HttpClient mClient = new DefaultHttpClient();
            try {
                HttpResponse response = mClient.execute(get);
                if (response.getStatusLine().getStatusCode() != 200) {
                    Log.d("error", "联网失败");
                    latLongString = "无法获取位置信息";
                } else {
                    HttpEntity entity = response.getEntity();
                    latLongString = EntityUtils.toString(entity, "gb2312");
                    JSONObject json = new JSONObject(latLongString);
                    latLongString = json.getString("dist");
                    System.out.println(latLongString);
                }

            } catch (Exception e) {
                Log.d("tag", "出错了");
                latLongString = "无法获取位置信息";
                e.printStackTrace();
            }
        } else {
            latLongString = "无法获取位置信息";
        }
        final String address = latLongString;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                address_txt.setText(address);
            }
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.b_get_verify) {
            phoneNum = et_reg_phone.getText().toString();
            if (isPhoneNum(phoneNum)) {
                //TODO 验证手机号码
            } else {
                setInfoPhone("请输入正确的手机号码");
            }
        } else if (id == R.id.b_reg_phone) {
            phoneNum = et_reg_phone.getText().toString();
            phone_psd = phone_reg_pwd.getText().toString();
            phone_name = phone_reg_username.getText().toString();
            String repwd = phone_reg_repwd.getText().toString();
            if (phoneNum.equals("")) {
                setInfoPhone("手机号不能为空");
                return;
            }
            if (isPhoneNum(phoneNum)) {
                if (phone_psd.equals(repwd)) {

                } else {
                    setInfoPhone("密码不一致");
                }
            } else {
                setInfoPhone("请输入正确的手机号码");
            }

        } else if (id == R.id.b_haveAccount) {
            startActivity(new Intent(RegisterAcrivity.this, LoginActivity.class));
            finish();
        }

    }

    public void setInfoPhone(String str) {
        if (tv_reg_info_phone != null)
            tv_reg_info_phone.setText(str);
    }

    private boolean isPhoneNum(String phone) {
        return phone.matches("^(13|15|18|17)\\d{9}$");
    }
}
