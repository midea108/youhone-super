package com.youhone.youhone_super.activity;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewConfiguration;

import com.youhone.superapp.R;
import com.youhone.youhone_super.base.BaseActivity;
import com.youhone.youhone_super.base.BaseFragment;
import com.youhone.youhone_super.fragment.DeviceListFragment;
import com.youhone.youhone_super.fragment.SettingFragment;
import com.youhone.youhone_super.fragment.ShoppingFragment;
import com.youhone.youhone_super.view.IconPagerAdapter;
import com.youhone.youhone_super.view.IconTabPageIndicator_Main;
import com.youhone.youhone_super.view.LazyViewPager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog.Builder;
import android.app.Dialog;

/**
 * Created by cgt on 2016/4/12.
 * 主界面
 */
public class MainActivity extends BaseActivity {

    public static int position = 0;
    private LazyViewPager mViewPager;
    private IconTabPageIndicator_Main mIndicator;
    private List<Fragment> mTabs = new ArrayList<Fragment>();
    private FragmentAdapter mAdapter;
    private Fragment deviceFragment, shoppingFragment, settingFragment;
    private Fragment currentFrag;
    private boolean isForgetBck = false;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_main);
    }

    @Override
    protected View initWidget() {
        setOverflowShowingAlways();
        mViewPager = (LazyViewPager) findViewById(R.id.view_pager);
        mIndicator = (IconTabPageIndicator_Main) findViewById(R.id.indicator);
        initFragments();
        mAdapter = new FragmentAdapter(mTabs, getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mViewPager);
        return null;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }


    public void webViewGoBack() {
        if (currentFrag != null && currentFrag instanceof ShoppingFragment) {
            System.out.println("DeviceListTab--商城网页返回");
            ((ShoppingFragment) shoppingFragment).goBack();
        } else {
            showExitDialog();
        }
    }

    private void initFragments() {

        deviceFragment = new DeviceListFragment();
        Bundle deviceargs = new Bundle();
        deviceargs.putString("title", "设备列表");
        deviceFragment.setArguments(deviceargs);
        ((BaseFragment) deviceFragment).setIconId(R.drawable.tab_device);
        ((BaseFragment) deviceFragment).setTitle("设备");
        mTabs.add(deviceFragment);

        shoppingFragment = new ShoppingFragment();
        Bundle shoppingargs = new Bundle();
        shoppingargs.putString("title", "商城页");
        shoppingFragment.setArguments(shoppingargs);
        ((BaseFragment) shoppingFragment).setIconId(R.drawable.tab_shopping);
        ((BaseFragment) shoppingFragment).setTitle("商城");
        mTabs.add(shoppingFragment);

        settingFragment = new SettingFragment();
        Bundle settingargs = new Bundle();
        settingargs.putString("title", "设置页");
        settingFragment.setArguments(settingargs);
        ((BaseFragment) settingFragment).setIconId(R.drawable.tab_my);
        ((BaseFragment) settingFragment).setTitle("我的");
        mTabs.add(settingFragment);
    }

    class FragmentAdapter extends FragmentStatePagerAdapter implements
            IconPagerAdapter {
        private List<Fragment> mFragments;

        public FragmentAdapter(List<Fragment> fragments, FragmentManager fm) {
            super(fm);
            mFragments = fragments;
        }

        @Override
        public Fragment getItem(int i) {
            return mFragments.get(i);
        }

        @Override
        public int getIconResId(int index) {
            return ((BaseFragment) mFragments.get(index)).getIconId();
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return ((BaseFragment) mFragments.get(position)).getTitle();
        }
    }

    private void setOverflowShowingAlways() {
        try {
            // true if a permanent menu key is present, false otherwise.
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class
                    .getDeclaredField("sHasPermanentMenuKey");
            menuKeyField.setAccessible(true);
            menuKeyField.setBoolean(config, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showExitDialog() {
        Builder builder = new Builder(this);
        builder.setCancelable(false);
        builder.setMessage("退出程序吗？");
        builder.setTitle("提示");
        builder.setNegativeButton("确定",
                new android.content.DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        System.exit(0);
                    }
                });
        builder.setNeutralButton("取消", null);
        builder.create().show();
    }

    public void showTipsDialog(String msg,
                               android.content.DialogInterface.OnClickListener listener) {
        Builder builder = new Builder(this);
        builder.setCancelable(false);
        builder.setMessage(msg);
        builder.setTitle("提示");
        builder.setNegativeButton("确定", listener);
        builder.setNeutralButton("取消", null);
        builder.create().show();
    }

}
