package com.youhone.youhone_super.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.youhone.superapp.R;
import com.youhone.youhone_super.base.BaseActivity;

/**
 * Created by cgt on 2016/4/11.
 * 登录界面
 */
public class LoginActivity extends BaseActivity {
    private EditText account, password;
    private Button login, register;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_login);
    }

    @Override
    protected View initWidget() {
        account = (EditText) findViewById(R.id.et_login_account);
        password = (EditText) findViewById(R.id.et_login_password);
        login = (Button) findViewById(R.id.b_login);
        register = (Button) findViewById(R.id.b_register);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doLogin(account.getText().toString(), password.getText().toString());
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterAcrivity.class));
                finish();
            }
        });
        return null;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void LoginSucceed(String account, String access_token) {
        super.LoginSucceed(account, access_token);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }
}
